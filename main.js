let voices;

// wait on voices to be loaded before fetching list
window.speechSynthesis.onvoiceschanged = function() {
    console.log("Voices loaded");
    voices = window.speechSynthesis.getVoices();
    console.log(voices);
};

// ui element references
let textarea = document.querySelector("#text");
let pronounceWords = document.querySelector("#pronounce-word");
let pronounceSentences = document.querySelector("#pronounce-sentence");

// regex for detecting end of sentences.
let endSentenceRegex = /[.!?]/;

function speakMessage(message)
{
    var msg = new SpeechSynthesisUtterance();
    msg.voice = voices[15]; // Note: some voices don't support altering params
    msg.voiceURI = 'native';
    msg.volume = 1; // 0 to 1
    msg.rate = 0.75; // 0.1 to 10
    msg.pitch = 1; //0 to 2
    msg.lang = 'nl-NL';
    let lastWord = message;
    msg.text = lastWord;
    console.log('say : ' + lastWord);
    speechSynthesis.speak(msg);
}

function pronounceWholeText()
{
    speakMessage(textarea.value);
}

// last text value
let lastValue = "";
function textChanged()
{
    let text = textarea.value;
    // ignore when text is still the same, function seems to be called twice sometimes.
    if(text == lastValue) return;
    lastValue = text;
    
    if(pronounceSentences.checked && endSentenceRegex.test(text[text.length-1]))
    {
        let sentences = text.split(/[.!?]/);
        // remove last element (since end of string is .)
        sentences.pop();
        // get last sentence from array
        let sentence = sentences.pop();
        speakMessage(sentence);
    }
    else if(pronounceWords.checked && text[text.length-1] == " ")
    {
        let parts = text.split(" ");
        // remove last element (since end of string is space)
        parts.pop();
        // get last word from array
        let word = parts.pop();

        if(endSentenceRegex.test(word[word.length-1]) && pronounceSentences.checked)
        {
            // ignore, since the sentence was just pronounced
            return;
        }
        speakMessage(word);
    }
}